import React from 'react'

import {
    Routes,
    Route,
    Navigate,
  } from "react-router-dom";
import { Login } from './components/Login';
import { PlanHome } from './components/PlanHome';

type AppRouter = {
    children: React.ReactNode;
}

const App = (props: AppRouter) => {
    return <>{props.children}</>
}




export const AppRoutes = () => {
    return (
        <App>
            <Routes>
                <Route path="/" element={<Navigate to="/Login" />} />
                <Route path="/login" element={<Login />} />
                <Route path="/plan" element={<PlanHome />} />
            </Routes>
        </App>
    )
}
