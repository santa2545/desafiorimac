import React from 'react'
import { Link, useLocation } from 'react-router-dom';
import { useCounter } from '../hooks/useCounter';
import { useForm } from '../hooks/useForm';
import { useState } from 'react';

export const PlanHome = () => {

  //vamos a desestructurar al objeto que viene de contadorconhooks

  const { valor, acumular } = useCounter(14300);

  const [validateLLanta, setValidateLlanta] = useState(false)
  const [validateChoqueLuz, setValidateChoqueLuz] = useState(false)
  const [validateAtropello, setValidateAtropello] = useState(false)


  let monto = 0;
  let montoBase = 20;
  let llantaRobada = validateLLanta ? 15 : 0;
  let choqueLuzRoja = validateChoqueLuz ? 20 : 0;
  let AtropelloVia = validateAtropello ? 50 : 0;

  

  

  console.log(valor);

  if(valor > 16000) {
    choqueLuzRoja = choqueLuzRoja - choqueLuzRoja;
  }else if(valor <= 16000){
    choqueLuzRoja = choqueLuzRoja;
  }

  monto = montoBase + llantaRobada + choqueLuzRoja +AtropelloVia;
  console.log(monto)
  

  const  btnvalidateLlanta =() => {

    setValidateLlanta(!validateLLanta)

  }

  const  btnvalidateChoque =() => {

    setValidateChoqueLuz(!validateChoqueLuz)


  }

  const  btnvalidateAtropello =() => {

    setValidateAtropello(!validateAtropello)


  }
  
  

  const { state } : any = useLocation();
  console.log(state);


  return (
    <div className='container--grid'>
        
        <nav className='nav-section d-none-movil'>
          <ul className="nav flex-column text-center">
            <li className="nav-item">
              <Link to='/' className="nav-link nav-text " aria-current="page" >Datos</Link>
            </li>
            <li className="nav-item">
              <Link to='/' className="nav-link  nav-text active" >Arma tu Plan</Link>
            </li>
          </ul>
        </nav>
        <section className='section-plan'>
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active" aria-current="page">Volver</li>
              </ol>
            </nav>
            <div style={{margin:'20px'}}>
              <h4 className='header-negro'>!Hola, <span style={{color: "red"}}>Juan!</span></h4>
              <label>Conocé las coberturas para tu plan</label>
              <div className="card mt-2">
                
                <div className="card-body">
                  <h6 className="card-subtitle mb-2 text-muted">Placa: {state.userData.Placa}</h6>
                  <h5 className="card-title">{state.userData.Marca + ' ' + state.userData.Modelo }</h5>
                  <h5 className="card-title">{state.userData.Año }</h5>
                </div>
              </div>

              <div className='flex mt-4'>
                <div className='col-md-7 col-6'>
                  <h6>Indica la suma asegurada</h6>
                  <span>Min $12.500 | Max $16.500</span>
                </div>
                <div className='col-md-5 col-6'>
                  <div className="input-group mb-3">
                    <button className="input-group-text" onClick={() => acumular(-100)}><span  >-</span></button>
                    <small className="form-control" >${valor}</small>
                    <button 
                      className="input-group-text" 
                      onClick={()=> acumular(+100)}
                    >
                        <span  >+</span>
                    </button>
                  </div>
                </div>
              </div>
              <hr />
              <div className='mt-4'>
                <h6>Agrega o quita coberturas</h6>
                  <div className="m-4">
                    <ul className="nav nav-tabs" id="myTab">
                        <li className="nav-item">
                            <a href="#proteccionAuto" className="nav-link active" data-bs-toggle="tab">Protege a tu auto</a>
                        </li>
                        <li className="nav-item">
                            <a href="#proteccionFamiliar" className="nav-link" data-bs-toggle="tab">Protege a los que te rodean</a>
                        </li>
                        <li className="nav-item">
                            <a href="#upgradePlan" className="nav-link" data-bs-toggle="tab">Mejora tu plan</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="proteccionAuto">
                            <div>
                              <h6 className="mt-2">Llanta Robada</h6>

                              {
                                validateLLanta ? 
                                  <>
                                      <button onClick={btnvalidateLlanta} className='btn-circular'>
                                      +
                                    </button>
                                    <span>Agregar</span>
                                    <p>Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate</p>
                                  </> : 
                                  <>
                                      <button onClick={btnvalidateLlanta} className='btn-circular'>
                                      -
                                    </button>
                                    <span>Quitar</span>
                                  </>
                              }
                              

                              
                            </div>
                            <hr />
                            <div>
                              <h6 className="mt-2">Choque y/o pasarte la luz roja</h6>
                              {
                                validateChoqueLuz ? 
                                  <>
                                      <button onClick={btnvalidateChoque} className='btn-circular'>
                                      +
                                    </button>
                                    <span>Agregar</span>
                                    <p>Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate</p>
                                  </> : 
                                  <>
                                      <button onClick={btnvalidateChoque} className='btn-circular'>
                                      -
                                    </button>
                                    <span>Quitar</span>
                                  </>
                              }

                            </div>
                            <hr />
                            <div>
                              <h6 className="mt-2">Atropello en la vida Evitamiento</h6>

                              {
                                validateAtropello ? 
                                  <>
                                      <button onClick={btnvalidateAtropello} className='btn-circular'>
                                      +
                                    </button>
                                    <span>Agregar</span>
                                    <p>Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate</p>
                                  </> : 
                                  <>
                                      <button onClick={btnvalidateAtropello} className='btn-circular'>
                                      -
                                    </button>
                                    <span>Quitar</span>
                                  </>
                              }
                              
                            </div>
                            
                        </div>
                        <div className="tab-pane fade" id="proteccionFamiliar">
                            <h4 className="mt-2">Protección Familiar</h4>
                            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut</p>
                        </div>
                        <div className="tab-pane fade" id="upgradePlan">
                            <h4 className="mt-2">Upgrade Plan</h4>
                            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit.</p>
                        </div>
                    </div>
                </div>
              </div>
              
            </div>
            
        </section>
        <aside className='mt-4'>
          <h4 className='header-negro'>Monto</h4>
          <h4 className='header-negro'>${monto}</h4>
          <h6>Mensuales</h6>
          <hr />

          <h6>El precio incluye:</h6>

          <ul>
            <li className='personalizado'>Llanta de repuesto</li>
            <li className='personalizado'>Análisis de motor</li>
            <li className='personalizado'>Aros gratis</li>
          </ul>

           <div className="text-center text-lg-start mt-4 mb-4 pt-2">
                           
                <button 
                    type="button" 
                    className="btn btn-danger btn-lg"
                    style={{paddingLeft: '2.5rem', paddingRight: '2.5rem'}}
                    onClick={()=>console.log("ir a la pantalla gracias")}
                >   
                    
                    LO QUIERO
                </button>

          </div>
        </aside>
    </div>
  )
}
