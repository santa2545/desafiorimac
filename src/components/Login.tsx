import React, { useEffect } from 'react'
import { useForm } from '../hooks/useForm';
import { useState } from 'react';
import axios from 'axios';
import { PersonaVehiculo } from '../interfaces/reqResApi';
import { useNavigate } from 'react-router-dom';

export const Login = () => {

    const navigate = useNavigate();

    const {formulario, onChange} = useForm({
        dni: '',
        celular: '',
        placa: ''
    })

    const [checkVal, setCheckVal] = useState(false);

    console.log(checkVal)

    const [personaVehiculo, setPersonaVehiculo] = useState([] as any);


    console.log("persona", personaVehiculo);

    console.log(personaVehiculo.length);

    const  checkValidate =() => {
        console.log("me diste click");
        console.log(checkVal);

        setCheckVal(!checkVal)

    }

    useEffect(() => {
      axios.get<PersonaVehiculo>('https://raw.githubusercontent.com/jsantamaria2505/mockjson/main/db.json')
        .then(resp => {
            console.log((resp.data))
            setPersonaVehiculo(resp.data);
            
        })
        .catch(err=>{
            console.log(err)
        })
    }, [])

    const verificarCotizar = () => {
        //const person = personaVehiculo.find(persona => persona.name.includes('')
        let persona; // Por defecto es undefined

        const dni = formulario.dni;
        const celular = formulario.celular;
        const placa = formulario.placa

        if(dni === '' || celular === '' || placa === '' || checkVal === false){
            alert("Por favor ingresar todos los campos, son necesarios");
            return;
        }

        for (let i = 0; i < personaVehiculo.length; i++) {
            const item = personaVehiculo[i];

            //console.log(dniIngresado);
            if (item.NroDocumento.includes(dni)) {
                persona = item;
                console.log("item persona", persona)

                if(persona.Placa === placa && persona.Celular === celular ){
                    console.log("navegar a la siguiente pantalla");
                    navigate("/plan", { state: { userData: persona } });
                    //window.location.href="/plan";
                    
                    
                }
                else{
                    alert("uno de los campos no coincide con el dni ingresado");
                }
                break;
            }else{
                console.log("Los datos ingresados no coinciden con nuestra bd, por favor asegurese de ingresar los datos correctamente")
            }
        }
    }

    
    

  return (
    <section className="vh-100">
        <div className="container-fluid h-custom">
            <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-md-9 col-lg-6 col-xl-5">
                <div>
                     <img src={"https://images.contentstack.io/v3/assets/bltc73a8adddb2104d0/blta78137d508afab4d/61df5575ef369648d52ebadc/VEHICULAR_hero.svg?auto=webp&quality=85&width=480"}
                            className="img-fluid" alt={"img"} />
                </div>
                <div>
                    <span>!Nuevo!</span>

                </div>
                <div>
                    <h4>Seguro <span>Vehicular </span></h4>
                    <h4>Tracking</h4>
                </div>
                <div>
                    <p>Cuentanos donde le haras seguimiento a tu seguro</p>
                </div>

                <div
                    className="d-flex flex-column d-none-movil flex-md-row text-center text-md-start justify-content-between py-2">

                    <div className="mb-3 mb-md-0">
                    Copyright © 2020. All rights reserved.
                    </div>

                </div>

               
            </div>
            <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                <div>
            

                    <div className="divider d-flex align-items-center justify-content-center my-4">
                        <p className="lead mb-0 me-3 fw-700">Déjanos tus datos</p>
                    </div>

                    <div className="form-outline mb-4">

                        <div className="input-group mb-3">
                            <select className="form-select">
                                <option  value={"1"}>DNI</option>

                            </select>
                            <input 
                                type="text" 
                                className="form-control form-control-lg" 
                                placeholder="Nro. de Doc" 
                                value= {formulario.dni}
                                onChange= {({target}) => onChange(target.value, 'dni')}
                                //onChange= {({value}) => onchange(value, 'dni')}
                            />
                        </div>
                        

                    </div>


                    <div className="form-outline mb-4">

                            <input 
                                className="form-control form-control-lg"
                                placeholder="Celular" 
                                value={formulario.celular}
                                onChange= {({target}) => onChange(target.value, 'celular')}
                            />
                    </div>


                    <div className="form-outline mb-3">
                        <input 
                            className="form-control form-control-lg"
                            placeholder="Placa" 
                            value={formulario.placa}
                            onChange= {({target}) => onChange(target.value, 'placa')}
                        />

                    </div>

                    <div className="d-flex justify-content-between align-items-center">

                        <div className="form-check mb-0">
                            <input className="form-check-input me-2" type="checkbox" value={checkVal ? 'checked' : 'unchecked'} onChange={checkValidate} />
                            <label className="form-check-label" >
                                <span>Acepto la </span>
                                <a href='/'>
                                    Política de Protección de Datos Personales
                                </a>
                                <span>y los </span>
                                <a href='/'>
                                    Términos y Condiciones
                                </a>

                            </label>

                            {
                                !checkVal ? <div className="alert alert-danger mt-2">Debe aceptar la politica de privacidad</div> : ''
                            }
                        </div>
                        
                    </div>

                    <div className="text-center text-lg-start mt-4 mb-4 pt-2">
                           
                                <button 
                                    type="button" 
                                    className="btn btn-danger btn-lg"
                                    style={{paddingLeft: '2.5rem', paddingRight: '2.5rem'}}
                                    onClick={verificarCotizar}
                                >   
                                    
                                    COTÍZALO
                                </button>
                           
                            

                        
                    </div>

                </div>
            </div>
            </div>
        </div>

        

        </section>
  )
}
