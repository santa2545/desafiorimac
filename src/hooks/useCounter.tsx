//Un hook no es más que una función

import { useState } from "react";

export const useCounter = ( inicial:number = 10) => {

    const [valor, setvalor] = useState(inicial);
    //Entre parentesis o como parametro el valor que desea incrementar
    const acumular = (numero: number) => {
        setvalor( valor + numero)
    }

    return {
        valor,
        acumular
    }
}
