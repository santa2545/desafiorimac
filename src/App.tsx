import React from 'react';
import {
  BrowserRouter as Router,
  Link, Navigate, Route, Routes,
} from "react-router-dom";
import { Login } from './components/Login';
import { PlanHome } from './components/PlanHome';

function App() {
  return (
    <Router >
      <div className="contenedor">
        <nav className='contenedor--nav h-20'>
          <div className='contenedor--nav--grid '>
            <div className='col-md-3 col-3 m-auto'>
                <Link to="/">
                  <div>
                    <img src={`https://images.contentstack.io/v3/assets/bltc73a8adddb2104d0/blt55de89b298bc8716/62056d8cfe1e3f147ecb52ff/Logo_RIMAC.svg?auto=webp&quality=85&width=108`}
                  className="img-fluid" alt={"img"} />
                  </div>
                </Link>
            </div>
            <div className='col-md-5 col-5 m-auto'>

            </div>
            <div className='col-md-4 col-4 m-auto'>
              <div className='flex'>
                <div className='col-md-6'>
                  <span className='w-dm d-none-movil'>¿Tienes alguna duda?</span>
                </div>
                <div className='col-md-6'>
                  <span className='w-dm d-none-movil primary-color fw-700'>(01) 411 6001</span>
                </div>

                <div className='col-md-6 col-4 m-auto d-none-desktop'>
                  <span className='w-dm primary-color fw-700'>Llámanos</span>
                </div>
                
                
              </div>
            </div>

          </div>
          
        </nav>
        
        <hr className='m-0'/>

        <div className=''>
              {
                /*<Link to="/" className='btn btn-dark'>
                  Inicio
                </Link>
                <Link to="/login" className='btn btn-dark'>
                  Login
                </Link>
                */
              }
              <Routes>
                  <Route path="/" element={<Navigate to="/Login" />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/plan" element={<PlanHome />} />
              </Routes>

          

        </div>
      </div>
    </Router>
      
      
    
      
    
    
  );
}

export default App;
